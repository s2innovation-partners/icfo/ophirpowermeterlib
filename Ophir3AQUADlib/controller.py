import logging
from socket import error, timeout
from telnetlib import Telnet
from threading import RLock

from Ophir3AQUADlib.resource import (
    lock_it,
    Ophir3AQUADException,
    convert_to_float,
    detect_error
)

CR = b"\x0d"
LF = b"\x0a"
DOLLAR = b"\x24"
STAR = b"\x2a"
ER = b"\x3f"
POSITION_MEASUREMENT_ERRORS = ["8000A000", "9000C000"]
SIZE_MEASUREMENT_ERRORS = ["90000001", "90000000"]


class Ophir3AQUADController:

    def __init__(self, host):
        self.host = host
        self.timeout = 3
        self.telnet = None
        self.lock = RLock()

    def connect(self):
        if not self.telnet:
            try:
                self.telnet = Telnet()
                self.telnet.open(host=self.host, timeout=self.timeout)
            except error:
                raise Ophir3AQUADException("Could not connect to device")

    @lock_it
    def disconnect(self):
        self.telnet.close()
        self.telnet = None

    def read_answer(self, cmd):
        with self.lock:
            try:
                self.telnet.write(DOLLAR + bytes(cmd, 'utf-8') + CR + LF)
            except timeout:
                raise Ophir3AQUADException("Connection timed out")
            except AttributeError:
                raise Ophir3AQUADException("Could not connect to device")

            self.telnet.read_until(CR + LF)
            answer = self.telnet.read_until(CR + LF)

        if ER in answer:
            answer = answer.split(ER)[1]
            answer = answer.split(CR)[0]
            raise Ophir3AQUADException(answer)

        if len(answer.split(CR)) > 2:
            raise Ophir3AQUADException("Read action return too many results")

        answer = answer.split(CR)[0]
        return answer

    @lock_it
    def ask(self, cmd):
        answer = self.read_answer(cmd)

        if not answer:
            raise Ophir3AQUADException("Cannot read answer")

        return answer

    def read_x_y_size_mm(self):
        values = self.ask("BT")

        _, _, error_code, _, x_coord, _, y_coord, _, size = values.split()

        x_coord, y_coord, size = self.position_and_size_measure(error_code, x_coord, y_coord, size)

        return x_coord, y_coord, size

    def read_x_y_size_microns(self):
        values = self.ask("BT 1")
        _, error_code, x_coord, y_coord, size = values.split()

        x_coord, y_coord, size = self.position_and_size_measure(error_code, x_coord, y_coord, size)

        return x_coord, y_coord, size

    def read_power(self):
        answer = self.ask("SP")
        power = answer.split(STAR)[1]
        return float(power)

    def check_connection(self):
        answer = self.ask("HP")
        if answer == STAR:
            logging.info("Correct connection")
        else:
            logging.error("Could not connect to device")

    def position_and_size_measure(self, error_code, _x_cord, _y_cord, _size):
        error_code_parsed = error_code.decode("utf-8")
        x_cord = 0
        y_cord = 0
        size = 0

        if error_code_parsed not in [*POSITION_MEASUREMENT_ERRORS, *SIZE_MEASUREMENT_ERRORS]:
            detect_error(error_code)

        if error_code_parsed not in POSITION_MEASUREMENT_ERRORS:
            x_cord = convert_to_float(_x_cord)
            y_cord = convert_to_float(_y_cord)

        if error_code_parsed not in SIZE_MEASUREMENT_ERRORS:
            size = convert_to_float(_size)

        return x_cord, y_cord, size
