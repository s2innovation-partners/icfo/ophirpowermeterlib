import logging
from functools import wraps


def lock_it(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        with self.lock:
            return func(self, *args, **kwargs)

    return wrapper


ERROR_DICT = {
    "UC": "Command is unknown",
    "BAD PARAM": "Incorrect parameters received",
    "NOT DEFINED": "Present value is undefined",
    "PARAM ERROR": "Incorrect parameter is sent",
    "FILTER SET UNAVAILABLE": "Invalid setting and sensor remains in previous filter mode",
    "NOT A PHOTO HEAD": "Sensor does not support this command",
    "NOT MEASURING ENERGY": "Sensor is not measuring energy but only power",
    "NOT USING CALIBRATION CURVE": "Sensor does not support a calibration curve",
    "INDEX NOT IN RANGE": "Index outside the range",
    "NO WL DEFINED AT INDEX": "Parameter is greater than 6",
    "WAVELENGTH OUT OF RANGE": "Wavelength outside the range",
    "WL ALREADY USED": "Chosen index already has a favorite wavelength defined for it",
    "WAVELENGTH ALREADY DEFINED. USE WL COMMAND": "Wavelength is already defined",
    "BAD WL": "Entered wavelength value is out of range of defined curve"
}


class Ophir3AQUADException(Exception):
    def __init__(self, answer=None):
        try:
            self.msg = ERROR_DICT[answer]
        except KeyError:
            self.msg = answer
        Exception.__init__(self, self.msg)

    def __str__(self):
        return self.msg


BEAM_DICT = {
"00000001": ("Warning", "Position Accuracy Warning (i.e. out of inner circle)"),
"00000002": ("Warning", "Spot Size too large for position measurement"),
"00000004": ("Warning", "Power Low (this might never be used)"),
"00001000": ("Error", "Position not measured (sensor can't measure position)"),
"00002000": ("Error", "Signal too low (i.e. signal is just noise, no meaningful measurements)"),
"00004000": ("Error", "Position Measurement out of range (i.e. out of outer circle)"),
"00008000": ("Error", "General Position Measurement Error"),
"00010000": ("Warning", "Size Accuracy Warning (i.e. distance from center / diameter > 20%)"),
"00020000": ("Warning", "Spot size less than EEROM limit"),
"00040000": ("Warning", "Spot size greater than EEROM limit"),
"10000000": ("Error", "Size not measured (head can't measure size)"),
"20000000": ("Error", "Negative Discriminant Or/And Negative Size"),
"40000000": ("Error", "Size Accuracy Error (i.e. distance from center / diameter > 33%)"),
"80000000": ("Error", "General Size Measurement Error"),
"8000A000": ("Error", "Position and sized not measured"),
"9000C000": ("Error", "Position and sized not measured"),
"90000001": ("Warning", "Size not measured"),
"90000000": ("Warning", "Size not measured")
}


def beam_track_errors(error):
    try:
        return BEAM_DICT[error.decode("utf-8")]
    except KeyError:
        raise Ophir3AQUADException("Unknown error in Beam Track")


def convert_to_float(string):
    try:
        return float(string)
    except ValueError:
        return 0.0


def detect_error(error_code):
    if error_code != "00000000":
        level, msg = beam_track_errors(error_code)

        if level == "Warning":
            logging.warning(msg)
        else:
            raise Ophir3AQUADException(msg)
