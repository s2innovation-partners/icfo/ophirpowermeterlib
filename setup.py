import setuptools

setuptools.setup(
    name="Ophir3AQUADlib",
    version="1.0.0",
    description="Library for Ophir 3A-QUAD sensor",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            "Ophir3AQUADlib=Ophir3AQUADlib:Ophir3AQUADController",
        ]
    }

)

